.. index:: Glossary

Glossary
********

.. glossary::

    ACF
      Autocorrelation function

    DHO
      Damped harmonic oscillator

    PSD
      Power spectral density

    SED
      Spectral energy density
