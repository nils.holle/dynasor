Tutorials
*********

A collection of short tutorials.

.. rubric:: Contents

.. toctree::
   :glob:
   :maxdepth: 1

   aluminum_liquid_dynamic
   aluminum_solid_dynamic
   static_structure_factor
   time_convergence
   acfs_and_ffts
   dho_peak_fitting
   sed
