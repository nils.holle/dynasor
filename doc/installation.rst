.. index:: Installation

Installation
************

Installation via ``pip`` or ``conda``
=====================================

Stable versions of :program:`dynasor` are provided via `PyPI <https://pypi.org/project/dynasor/>`_ and as part of `conda-forge <https://anaconda.org/conda-forge/dynasor/>`_.
This implies that :program:`dynasor` can be installed using ``pip``::

    pip install dynasor

or using ``conda``::

    conda install -c conda-forge dynasor


Installation via ``setup.py``
=============================
.. index:: Git repository

If you want to use the most recent (development) version you can clone the repository::
	   
   git clone https://gitlab.com/materials-modeling/dynasor.git

and subsequently install using the ``setup.py`` script while standing in the ``dynasor`` directory as follows::

    pip install --user .

or::

    python3 setup.py install --user


Requirements
============

:program:`dynasor` requires :program:`Python` 3.8+ and depends on the following libraries:

* `ASE <https://wiki.fysik.dtu.dk/ase>`_ (trajectory reading)
* `MDAnalysis <https://www.mdanalysis.org/>`_ (trajectory reading)
* `NumPy <http://www.numpy.org/>`_ (numerical linear algebra)
* `Numba <https://numba.pydata.org/>`_ (computational efficiency)
* `pandas <https://pandas.pydata.org/>`_ (data handling)


numba and icc_rt
================
:program:`dynasor` employs :program:`numba` for the efficient calculation of correlation functions.
To get the full benefit of using :program:`numba` (`see here <https://numba.pydata.org/numba-doc/latest/user/performance-tips.html#intel-svml>`_) you need to install :program:`icc_rt`, which can be installed using, e.g., ``conda`` or ``pip``.
:program:`icc_rt` can lead to certain use cases running 5 to 10 times faster.

Note that there is an existing bug when installing :program:`icc_rt` with ``pip`` (see this `github issue <https://github.com/numba/numba/issues/4713>`_).
If you run ``numba -s`` the ideal output is::

    __SVML Information__
    SVML State, config.USING_SVML                 : True
    SVML Library Loaded                           : True
    llvmlite Using SVML Patched LLVM              : True
    SVML Operational                              : True

If any of these are ``False`` you might have to add the python library directory to your ``LD_LIBRARY_PATH``.
For example if you installed ``numba`` in your local user-owned python directotry, the following line could work::

    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/.local/lib

If this does not resolve the issue or if want to read up on the background you may want to consult the aforementioned `github issue <https://github.com/numba/numba/issues/4713>`_.
