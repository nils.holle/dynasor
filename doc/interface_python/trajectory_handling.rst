.. index::
   single: Function reference; Trajectory handling

Trajectory handling
*******************

.. autoclass:: dynasor.Trajectory
   :members:
   :inherited-members:
