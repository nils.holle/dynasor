#!/bin/sh

#
# Options
Q_BINS=21
TIME_WINDOW=500
MAX_FRAMES=10000
Q_POINTS=21

dt=50 # time difference between to frames in the trajectory file 

q=31.4159265359 # 2pi/a in nanometer^-1

TRAJECTORY="lammpsrun/dump_T10"
OUTPUT="outputs/dynasor_1Dchain"
INDEXFILE="index.1000"

dynasor -f "$TRAJECTORY" \
	--index=$INDEXFILE \
	--om=$OUTPUT.m \
	--op=$OUTPUT.pickle \
    --q-bins=$Q_BINS \
	--max-frames=$MAX_FRAMES \
	--nt=$TIME_WINDOW \
	--dt=$dt \
	--q-sampling="line" \
	--q-points=$Q_POINTS \
	--q-direction=$q,0,0


























