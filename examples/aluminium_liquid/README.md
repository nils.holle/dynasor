# Example: Dynamic properties of liquid aluminium

This directory contains files connected to an example illustrating the dynamic properties of liquid aluminum.
The molecular dynamics trajectory that is analuzed here can be downloaded (https://zenodo.org/records/10013486/files/dumpT1400.NVT.atom.velocity) or generated by running lammps using the input files in the `lammpsrun` directory.
Please consult the [lammps documentation](https://www.lammps.org) for information on how to compile and run lammps.

The analysis of the trajectory using **dynasor** is illustrated in the notebook.
